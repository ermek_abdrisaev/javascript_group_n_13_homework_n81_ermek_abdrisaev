import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { LinkService } from '../shared/link.service';
import { Link, LinkData } from '../shared/link.model';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-link',
  templateUrl: './link.component.html',
  styleUrls: ['./link.component.sass']
})
export class LinkComponent implements OnInit {
  @ViewChild('f') form!: NgForm;
  links: Link[] = [];
  shorten!: any;
  apiUrl = environment.apiUrl;

  constructor(private linkService: LinkService) { }

  ngOnInit(): void {}

  onSubmit(){
   const linkData: LinkData = this.form.value;
    this.linkService.postLink(linkData).subscribe( shorten => {
      this.shorten = shorten;
    })
  }

  clickUrl() {
    this.linkService.getLink(this.shorten).subscribe();
  }
}
