import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LinkData } from './link.model';
import { environment } from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class LinkService {

  constructor(private http: HttpClient) { }

  getLink(short: string){
    return this.http.get(environment.apiUrl + `/links/` + short);
  }

  postLink(linkData: LinkData){
    return this.http.post(environment.apiUrl + '/links', linkData);
  }

}
