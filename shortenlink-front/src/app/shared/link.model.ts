export class Link {
  constructor(
    public linkUrl: string,
    public _id: string,
    public shortenedLink: string,
  ) {}
}

export interface LinkData {
  [key: string]: any;
  shortenedLink: string,
}
