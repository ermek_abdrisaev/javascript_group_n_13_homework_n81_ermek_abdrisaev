const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const LinkSchema = new Schema({
  link: {
    type: String,
    required: true,
  },
  shortenedLink: {
    type: String,
  }
});

const Link = mongoose.model('link', LinkSchema);

module.exports = Link;