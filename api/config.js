const path = require('path');

const rootPath = __dirname;

module.exports ={
  rootPath,
  mongoConfig: {
    db: 'mongodb://localhost/links',
    options: {useNewUrlParser: true},
  }
}