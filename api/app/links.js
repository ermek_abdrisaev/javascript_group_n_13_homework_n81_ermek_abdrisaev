const express = require('express');
const { nanoid} = require('nanoid');
const Link = require('../models/Links');

const router = express.Router();

router.get('/:short', async (req, res, next ) =>{
  try{
    const linkUrl = await Link.findOne({shortenedLink: req.params.short});
    const fullUrl = linkUrl.link;

    if(!linkUrl){
      return res.status(404).send({message: "Link not found"});
    }
    res.status(301).redirect(fullUrl);

  } catch (e) {
    next(e);
  }
});


router.post('/', async (req, res, next) =>{
  try{

    if(!req.body.linkUrl){
      return res.status(404).send({message: 'Enter link'});
    }

    const linkData = {
      link: req.body.linkUrl,
      shortenedLink: nanoid(6)
    };

    const linkUrl = await new Link(linkData);
    await linkUrl.save();

    return res.send(JSON.stringify(linkData.shortenedLink));
  } catch (e) {
    next(e);
  }
});


module.exports = router;